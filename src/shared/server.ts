function evenOrDood(num1: number): void {
  console.log(num1 % 2 == 0 ? 'Par' : 'Impar');
}

function compare(_enteredText: string): void {
  const reverse = _enteredText.split('').reverse().join('');
  console.log(_enteredText === reverse ? 'Palindromo' : 'N-Palindromo');
}

function anagram(_enteredText1: string, _enteredText2: string): void {
  if (_enteredText1.length != _enteredText2.length) {
    console.log('Palavras de tamanhos diferentes');
    return;
  }
  const auxText1 = _enteredText1.split('').sort().join('');
  const auxText2 = _enteredText2.split('').sort().join('');
  console.log(auxText1 === auxText2 ? 'Palindromo' : 'N-Palindromo');
}

function asciiWords(enteredText: string) {
  const r = enteredText.split('').map((ele: string) => ele.charCodeAt(0));
  const ascii = [];
  for (let i = 0; i < enteredText.length; i++) {
    ascii.push(`${enteredText[i]} = ${r[i]}`);
  }
  console.log('Array ==>', ascii);
}
